import React from 'react';
import Button from '@material-ui/core/Button';

const App = () => {
	return (
		<Button variant="contained" color="primary">
			Hello material!
		</Button>
	);
};

export default App;
