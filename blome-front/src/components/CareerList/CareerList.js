import React from 'react';
import styles from './CareerList.scss';
import classNames from 'classnames/bind';

const cx = classNames.bind(styles);

const CareerList = () => {
	<div className={cx('career-list')}>
		CareerList
	</div>
};

export default CareerList;
