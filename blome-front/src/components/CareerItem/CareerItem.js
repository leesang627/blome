import React from 'react';
import styles from './CareerItem.scss';
import classNames from 'classnames/bind';

const cx = classNames.bind(styles);

const CareerItem = () => {
	<div className={cx('career-item')}>
		CareerItem
	</div>
};

export default CareerItem;
